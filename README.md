Watch The Blacklist Season 6 Episode 22 Online Full Free. As the president's true plan comes into focus, Liz and the task force fight to avert disaster; Red meets in secret with a man who holds information about his past.

[Click Here to watch Full Episode Now](https://tv.streamid.online/tv/46952/the-blacklist/6/22)

[Click Here to watch Full Episode Now](https://tv.streamid.online/tv/46952/the-blacklist/6/22)

The Blacklist Season 6 Episode 22 bears the title of the US President's name Robert Diaz (Benito Martinez). As the Task Force will deal with the POTUS's real plan that is about to go wrong, the season finale may also reveal some details about the real Red (James Spader).

"As the President' true plan comes into focus, Liz (Megan Boone) and the Task Force fight to avert disaster," The Blacklist Season 6 Episode 22 official synopsis read. "Meanwhile, Red meets in secret with a man who holds information about his past."

The Blacklist Season 6 Episode 22 will continue where Episode 21, "Anna McMahon," left off. The Task Force learned the Secret Service's conspiracy to assassinate President Diaz, and things would be very intense in the season finale.

"She's not going to stop before we are dead," Red can be heard saying to someone in The Blacklist Season 6 Episode 22 trailer. The next scene reveals a montage of explosions, gunshots, and men wearing masks storming a building. The clip ended with Anna McMahon (Jennifer Ferrin) pointing a gun on Red's head and said, "no more cheating death."

According to Express.co.uk, fans are hoping The Blacklist Season 6 Episode 22 will give answers to a lot of questions. Viewers want to know what illness Red has, as he has been continuously looking for a cure from "someone." There are theories that someone is also him as he was seen injecting himself something while telling Liz he wanted to live a healthier life.

Liz's mother, Katarina Rostova (Lotte Verbeek), is also revealed to be alive. So, would there be a chance he would be seen in The Blacklist Season 6 Episode 22? There are also questions about Anna's anger toward Red, and why she wants to see him dead.

Talking about Red, Cinema Blend reported Blacklist Season 6 Episode 22 might reveal something about Red's real identity. Red would reportedly meet a man who has the knowledge about the real Red's past. The guy's identity remains to a big mystery, but there are also questions if he would give any info about Ilya Koslov (Gabriel Mann), a former Russian agent and Katarina's friend.

Blacklist Season 6 Episode 22, Robert Diaz, will be out on Friday, May 17, at 8 p.m. ET on NBC. The network already renewed the show for Season 7.

Elizabeth Keen will serve as The Blacklist‘s own Obi-Wan Kenobi when tonight’s season finale (NBC, 8/7c) gets underway.

Indeed, Liz will be the task force’s only hope, seeing as Cooper, Ressler and Aram were last seen in handcuffs, courtesy of Anna McMahon.

“One of the fun things in this episode is we start in great jeopardy,” series creator Jon Bokenkamp tells us ahead of the finale. “There’s a lot on Liz’s shoulders. It is now on her to somehow come up with a plan to activate our people again, and try to thwart — or even figure out — what this plot is that Anna McMahon and the president are planning.”

Though the details of Anna’s scheme with President Diaz have yet to be revealed to Blacklist viewers, Bokenkamp says their motives will come to light during the “fast-moving” season ender.

“At the moment, all we know is that the president is supportive of a plot to assassinate the president,” he says with a laugh. “Why would he do that? What is their plan? The ultimate answer that we’re going to unlock is a really surprising one.”

Meanwhile, the season-long arc about Red’s real identity — including what Liz claims to know about who Red is — will also come into play throughout the finale.

Per executive producer John Eisendrath, the episode will “spark new questions” about Red and Liz’s relationship, especially now that Liz believes Red is actually Ilya Koslov.

“The story we’ve told this year, of Liz’s quest to find out Raymond Reddington’s identity — and the answer that Dom gave her in Episode 19 — is one that brings Liz even closer to Raymond Reddington, not farther apart from him,” Bokenkamp adds. “He never liked the idea that she was looking into this, he has made that clear from the beginning… What that portends, going forward, is continued friction on the question of his real identity.”